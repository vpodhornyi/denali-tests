module.exports = {
  testName: 'autoTest',
  login: 'admin',
  password: 'admin',
  goto: 'localhost:3001',
  welcomeName: 'admin',
  redis: {
    host: '127.0.0.1',
    port: 6380,
    db: 10,
    instance: "idx",
  },
  database: {
    host: '127.0.0.1',
    port: 3307,
    user: 'den',
    password: 'den',
    database: 'den',
    timezone: 'Z',
  }
};