import qc from './queries/index';

const query = require('./index').query;

const sqlTargeting = async (dspId, targetType) => {

  const data = await query(qc.getDspTargeting(dspId, targetType), true);

  const id = data.dsp_id;
  const type = data.type;
  const res = {};

  res.active = data.active ? '1' : '0';
  res['allow.domain'] = '0';
  res['deny.domain'] = '0';
  res['allow.geo.country'] = '';
  res['deny.geo.country'] = '';
  res['allow.ip'] = '0';
  res['deny.ip'] = '0';
  res['allow.mime'] = '';
  res['deny.mime'] = '';
  res['allow.devicetype'] = '';
  res['allow.player.size'] = '';

  return res;
};

module.exports = {
  sqlTargeting,
};