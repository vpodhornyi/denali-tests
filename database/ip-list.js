import qc from './queries/index';

const query = require('./index').query;

const sqlIps = async (dspId) => {

  const data = await query(qc.getIps(dspId));

  const res = {};

  for(let key in data){
    for(let foo in data[key]){
      res[key] = data[key][foo]
    }
  }

  return res;
};

module.exports = {
  sqlIps,
};