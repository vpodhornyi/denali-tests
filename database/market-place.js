import moment from 'moment';
import qc from './queries/index';

const query = require('./index').query;

const sqlMarketPlace = async (id) => {
  const data = await query(qc.getDeals(id), true);

  const res = {};

  if (data.time_start) {
    res['time.start'] = moment(data.time_start).utc().format();
  }

  if (data.time_end) {
    res['time.end'] = moment(data.time_end).utc().format();
  }

  res['external.id'] = data.external_id.toString();
  res.demand = data.dsp_id.toString();
  res.supply = data.name;
  res.active = data.active.toString();
  res.type = data.type;
  res.margin = data.margin.toString();
  res.cpm = data.cpm.toString();
  res.share = (data.share/100).toString();

  return res
};

module.exports = {
  sqlMarketPlace,
};
