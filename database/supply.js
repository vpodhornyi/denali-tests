import qc from './queries/index';

const query = require('./index').query;

const sqlSsp = async (id, type) => {

  let data = {};
  let margin = {};
  const res = {};

  switch (type) {
    case 'Roku':
      data = await query(qc.getSspRoku(id), true);
      break;
    case 'OpenRTB':
      data = await query(qc.getSspOpenRTB(id), true);
      margin = await query(qc.getMarginFloor(data.margin_floor_id), true);

      break;
  }

  res.publisher_id = data.publisher_id.toString();
  res.active = data.active.toString();
  res.qps = data.qps_limit ? data.qps_limit.toString() : '-1';
  res.type = data.type === 1 ? 'CTV' : 'SSP';
  res['mandatory'] = '';
  res.protocol = data.protocol_version || '';
  res['auction.type'] = 'SECOND';
  res['cookie.id'] = '';
  res['mandatory.adm'] = '';
  res['mandatory.nurl'] = '';
  res['price.encrypt'] = data.price_enc_method || '';
  res['price.key'] = data.price_enc_secret || '';
  res.source = '';
  res.currency = data.currencies && data.currencies !== 'null' ? JSON.parse(data.currencies).join() : '';

  res['dsp.m.ctv'] = data.demand_margin ? data.demand_margin.toString() : margin.demand_ctv ? margin.demand_ctv.toString() : 'err';
  res['ssp.m.ctv'] = data.supply_margin ? data.supply_margin.toString() : margin.supply_ctv ? margin.supply_ctv.toString() : 'err';
  res['min.m.ctv'] = data.default_bid_floor ? data.default_bid_floor.toString() : margin.bid_floor_ctv ? margin.bid_floor_ctv.toString() : 'err';

  if (res.type === 'SSP') {
    res['dsp.m.banner'] = margin.demand_desktop_site_banner.toString();
    res['dsp.m.video'] = margin.demand_desktop_site_video.toString();
    res['dsp.m.app.banner'] = margin.demand_desktop_app_banner.toString();
    res['dsp.m.app.video'] = margin.demand_desktop_app_video.toString();
    res['dsp.m.mob.banner'] = margin.demand_mobile_site_banner.toString();
    res['dsp.m.mob.video'] = margin.demand_mobile_site_video.toString();
    res['dsp.m.mob.app.banner'] = margin.demand_mobile_app_banner.toString();
    res['dsp.m.mob.app.video'] = margin.demand_mobile_app_video.toString();

    res['ssp.m.banner'] = margin.supply_desktop_site_banner.toString();
    res['ssp.m.video'] = margin.supply_desktop_site_video.toString();
    res['ssp.m.app.banner'] = margin.supply_desktop_app_banner.toString();
    res['ssp.m.mob.banner'] = margin.supply_mobile_site_banner.toString();
    res['ssp.m.app.video'] = margin.supply_desktop_app_video.toString();
    res['ssp.m.mob.video'] = margin.supply_mobile_site_video.toString();
    res['ssp.m.mob.app.banner'] = margin.supply_mobile_app_banner.toString();
    res['ssp.m.mob.app.video'] = margin.supply_mobile_app_video.toString();

    res['min.m.banner'] = margin.bid_floor_desktop_site_banner.toString();
    res['min.m.video'] = margin.bid_floor_desktop_site_video.toString();
    res['min.m.app.banner'] = margin.bid_floor_desktop_app_banner.toString();
    res['min.m.app.video'] = margin.bid_floor_desktop_app_video.toString();
    res['min.m.mob.banner'] = margin.bid_floor_mobile_site_banner.toString();
    res['min.m.mob.video'] = margin.bid_floor_mobile_site_video.toString();
    res['min.m.mob.app.banner'] = margin.bid_floor_mobile_app_banner.toString();
    res['min.m.mob.app.video'] = margin.bid_floor_mobile_app_video.toString();
  }

  return res;
};

module.exports = {
  sqlSsp,
};
