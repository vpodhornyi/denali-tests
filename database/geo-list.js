import qc from './queries/index';
const iso = require('iso-3166-1');

const query = require('./index').query;

const sqlCountries = async (id) => {

  const data = await query(qc.getCountries(id), true);

  const res = JSON.parse(data.countries);

  let countries = res ? res.map((item) => {
    if (item === 'NS') {
      return 'NONE';
    }

    const alphaObj = iso.whereAlpha2(item);

    if (alphaObj) {
      return alphaObj.alpha3;
    }

    return undefined;

  }) : [];

  return countries;
};

module.exports = {
  sqlCountries
};