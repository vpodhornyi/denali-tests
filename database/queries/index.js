const SQL = require('sql-template-strings');

module.exports = {
  getDeals: id => SQL`
    SELECT 
    dsp_id,
    time_start,
    time_end,
    external_id, 
    ds.active, 
    ds.type, 
    ds.margin,
    ds.cpm, 
    ds.share_of_voice share, 
    ss.name
    FROM deals ds
    JOIN ssps ss
    ON ds.ssp_id = ss.id
    WHERE ds.id = ${id}`,
  getDspAdSever: id => SQL`
    SELECT
    name,
    active,
    banner_traffic,
    video_traffic,
    native_traffic, 
    qps_limit,
    endpoint_url,
    region,
    type
    FROM dsps    
    WHERE id = ${id}`,
  getDspOpenRTB: id => SQL`
    SELECT
    ds.name,
    ds.active,
    ds.banner_traffic,
    ds.video_traffic,
    ds.native_traffic, 
    ds.qps_limit,
    ds.endpoint_url,
    ds.region,
    ds.type,
    ot.protocol_version
    FROM dsps ds    
    JOIN open_rtb_dsp_ext ot
    ON ds.id = ot.dsp_id
    WHERE ds.id = ${id}`,
  getSspRoku: id => SQL`
    SELECT
    ss.publisher_id,
    ss.name,
    ss.active,
    ss.qps_limit,
    ss.type,
    st.demand_margin,
    st.supply_margin,
    st.default_bid_floor
    FROM ssps ss
    JOIN roku_ssp_ext st
    ON ss.id = st.ssp_id
    WHERE ss.id =${id}`,
  getSspOpenRTB: id => SQL`
    SELECT
    ss.publisher_id,
    ss.name,
    ss.active,
    ss.qps_limit,
    ss.type,
    ot.protocol_version,
    ot.auction_type,
    ot.currencies,
    ot.price_enc_method,
    ot.price_enc_secret,
    ot.margin_floor_id
    FROM ssps ss
    JOIN open_rtb_ssp_ext ot
    ON ss.id = ot.ssp_id
    WHERE ss.id =${id}`,
  getMarginFloor: id => SQL`
  SELECT *
  FROM margin_floor
  WHERE id = ${id}`,
  getDspTargeting: (id, type) => SQL`
  SELECT *
  FROM dsp_targetings
  WHERE dsp_id = ${id}
  AND type = ${type}`,
  getIdsOfDomainsIps: id => SQL`
   SELECT dsp_id, dl.id dl_id, dt.type dt_type, il.id il_id,
  dsps.native_traffic, dsps.banner_traffic, dsps.video_traffic
  FROM dsps
  LEFT JOIN  dsp_targetings dt
  ON dsps.id = dt.dsp_id
  LEFT JOIN  domain_lists dl
  ON dt.id = dl.targeting_id
  LEFT JOIN domains ds
  ON dl.id = ds.domain_list_id
  LEFT JOIN  ip_lists il
  ON dt.id = il.targeting_id
  LEFT JOIN ips
  ON il.id = ips.ip_list_id
  WHERE dsps.id=${id}`,
  getDomains: id => SQL`
  SELECT domain
  FROM domains
  WHERE domain_list_id=${id}`,
  getIps: id => SQL`
  SELECT ip
  FROM ips
  WHERE ip_list_id=${id}`,
  getCountries: (id) => SQL`
  SELECT countries
  FROM country_lists
  WHERE id=${id}`
};
