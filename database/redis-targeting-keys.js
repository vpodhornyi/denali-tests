import { _ }from 'lodash';
import qc from './queries/index';

const query = require('./index').query;

const getRedisKeys = async (id) => {

  let arrIds = await query(qc.getIdsOfDomainsIps(id));

  arrIds = arrIds.map(obj => {
    return Object.entries(obj);
  });

  let result = [];
  let dsps_id = 0;

  arrIds.forEach(arr => {
    arr.forEach(item => {
      switch (item[0]) {
        case 'dl_id':
          if (item[1] !== null)
            result.push(`idx:domain:${item[1]}`);
          break;
        case 'il_id':
          if (item[1] !== null)
            result.push(`idx:ip:${item[1]}`);
          break;
        case 'dsps_id':
          if (item[1] !== null)
            dsps_id = item[1];
          break;
        case 'dt_type':
          if (item[1] !== null)
            result.push(`idx:target:demand:${dsps_id}:${item[1]}`);
          break;
        case 'native_traffic':
          if (item[1] === 0)
            result.push(`idx:target:demand:${dsps_id}:NATIVE`);
          break;
        case 'banner_traffic':
          if (item[1] === 0)
            result.push(`idx:target:demand:${dsps_id}:BANNER`);
          break;
        case 'video_traffic':
          if (item[1] === 0)
            result.push(`idx:target:demand:${dsps_id}:VIDEO`);
          break;
      }
    });
  });
  return _.uniq(result);
};