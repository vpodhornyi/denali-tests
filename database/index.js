import mysql from 'mysql';
import config from '../config/server';

const connection = mysql.createConnection(config.database);

const getQuery = (query, single) => new Promise((resolve, reject) => {
  connection.query(query, (error, results, fields) => {
    if (error) {
      reject(error);
      return;
    }
    resolve(single ? results[0] : results, fields);
  });
});

module.exports.connection = connection;
module.exports.query = getQuery;
