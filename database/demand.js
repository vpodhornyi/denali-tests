import qc from './queries/index';

const query = require('./index').query;

const sqlDsp = async (id, type) => {

  let data = {};
  const res = {};

  switch (type){
    case 'AdServer':
      data = await query(qc.getDspAdSever(id), true);
      break;
    case 'OpenRTB':
      data = await query(qc.getDspOpenRTB(id), true);
      break;
  }

  switch (data.type) {
    case 1:
      res.type = 'ADSERVER';
      break;
    case 2:
      res.type = 'DSP';
      break;
  }

  res.name = data.name;
  res.active = data.active.toString();
  res['allow.banner'] = data.banner_traffic.toString();
  res['allow.video'] = data.video_traffic.toString();
  res['allow.native'] = data.native_traffic.toString();
  res['cookie.id'] = '';
  res.qps = data.qps_limit ? data.qps_limit.toString() : '-1';
  res.url = data.endpoint_url;
  res.region = data.region;
  res.protocol = data.protocol_version ? data.protocol_version.toString() : '';

  return res;
};

module.exports = {
  sqlDsp,
};

