import qc from './queries/index';

const query = require('./index').query;

const sqlDomains = async (id) => {

  const data = await query(qc.getDomains(id));

  const res = {};

  for(let key in data){
    for(let foo in data[key]){
      res[key] = data[key][foo]
    }
  }
  
  return res;
};

module.exports = {
  sqlDomains,
};