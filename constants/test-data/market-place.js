module.exports = {
  TEST_1: 'Test_1: External ID generate automatically',//----Pass
  TEST_2: 'Test_2: External ID manual type',//---------------Pass
  TEST_3: 'Test_3: "Active" Off',//--------------------------Pass
  TEST_4: 'Test_4: "Start now" Off',//-----------------------Pass
  TEST_5: 'Test_5: "Don\'t expire" Off',//-------------------Pass
  TEST_6: 'Test_6: "Start now" and "Don\'t expire" Off',//---Pass
};