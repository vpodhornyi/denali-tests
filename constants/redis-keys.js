import conf from "../config/server";


module.exports = {
  DSP:`${conf.redis.instance}:demand:`,
  SSP: `${conf.redis.instance}:supply:`,
  DEAL: `${conf.redis.instance}:deal:`,
  TARGETING: `${conf.redis.instance}:target:demand:`,
  DOMAIN: `${conf.redis.instance}:domain:`,
  IP: `${conf.redis.instance}:ip:`,
};