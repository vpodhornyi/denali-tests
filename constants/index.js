import DOM from './dom';
import DSP from './demand';
import SSP from './supply';
import MP from './market-place';
import TARGETING from './targeting';
import KEYS from './redis-keys';
import {sqlMarketPlace} from '../database/market-place';

const compare = '------- Comparison of MySql and Redis data';
const archive = '------- Archived';
const del = '------- Deleted';
const del_check = '------- Redis key delete check';

module.exports = {
  DOM,
  DSP,
  SSP,
  MP,
  TARGETING,
  KEYS,
  COMPARE: compare,
  ARCHIVED: archive,
  DELETE: del,
  DELETE_CHECK: del_check,
  sqlMarketPlace,
};