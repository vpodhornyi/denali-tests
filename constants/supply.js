module.exports = {

  /*************** DIV *******************/
  DIV: {
    PROTOCOL: '.protocol_version',
    PRICE_ENC_METHOD: '.price_enc_method',
    CURRENCIES: 'div[class="Select is-clearable Select--multi"]',
    PUBLISHER: 'div[class="Select-placeholder"]',
  },

  /*************** BUTTON *******************/
  BUTTON: {
    NEW_SSP: 'button[class="btn control-btn sites"]',
  },

  /*************** LINK *******************/
  A: {
    SSPS: 'a[href="/supply/ssps"]',
    SUPPLY_SIDEBAR: 'a[href="/supply"]',
    MARGINS: 'a[href="/supply/ssps/8/margins"]',
  },

  /*************** INPUT *******************/
  INPUT: {
    NAME: '#name',
    PRICE_ENC_SECRET: '#price_enc_secret',
    DEMAND_MARGIN: '#demand_margin',
    SUPPLY_MARGIN: '#supply_margin',
    DEFAULT_BID_FLOOR: '#default_bid_floor',
    DEMAND_CTV: '#demand_ctv',
    DEMAND_DESKTOP_SITE_BANNER: '#demand_desktop_site_banner',
    DEMAND_DESKTOP_SITE_VIDEO: '#demand_desktop_site_video',
    DEMAND_DESKTOP_APP_BANNER: '#demand_desktop_app_banner',
    DEMAND_DESKTOP_APP_VIDEO: '#demand_desktop_app_video',
    DEMAND_MOBILE_SITE_BANNER: '#demand_mobile_site_banner',
    DEMAND_MOBILE_SITE_VIDEO: '#demand_mobile_site_video',
    DEMAND_MOBILE_APP_BANNER: '#demand_mobile_app_banner',
    DEMAND_MOBILE_APP_VIDEO: '#demand_mobile_app_video',
    SUPPLY_CTV: '#supply_ctv',
    SUPPLY_DESKTOP_SITE_BANNER: '#supply_desktop_site_banner',
    SUPPLY_DESKTOP_SITE_VIDEO: '#supply_desktop_site_video',
    SUPPLY_DESKTOP_APP_BANNER: '#supply_desktop_app_banner',
    SUPPLY_DESKTOP_APP_VIDEO: '#supply_desktop_app_video',
    SUPPLY_MOBILE_SITE_BANNER: '#supply_mobile_site_banner',
    SUPPLY_MOBILE_SITE_VIDEO: '#supply_mobile_site_video',
    SUPPLY_MOBILE_APP_BANNER: '#supply_mobile_app_banner',
    SUPPLY_MOBILE_APP_VIDEO: '#supply_mobile_app_video',
    BID_FLOOR_CTV: '#bid_floor_ctv',
    BID_FLOOR_DESKTOP_SITE_BANNER: '#bid_floor_desktop_site_banner',
    BID_FLOOR_DESKTOP_SITE_VIDEO: '#bid_floor_desktop_site_video',
    BID_FLOOR_DESKTOP_APP_BANNER: '#bid_floor_desktop_app_banner',
    BID_FLOOR_DESKTOP_APP_VIDEO: '#bid_floor_desktop_app_video',
    BID_FLOOR_MOBILE_SITE_BANNER: '#bid_floor_mobile_site_banner',
    BID_FLOOR_MOBILE_SITE_VIDEO: '#bid_floor_mobile_site_video',
    BID_FLOOR_MOBILE_APP_BANNER: '#bid_floor_mobile_app_banner',
    BID_FLOOR_MOBILE_APP_VIDEO: '#bid_floor_mobile_app_video',
  },

  /*************** LABEL *******************/
  LABEL: {
    DEFAULT_VALUES: '#label-use_default_margin_floor',
  },
  /*************** TEXTAREA *******************/
  TEXTAREA: {
    COMMENTS: '.comments',
  },
};



