module.exports = {

  /*************** DIV *******************/
  DIV: {
    DSP: '.dsp_id',
    SSP: '.ssp_id',
    PROTOCOL: '.protocol_version',
  },

  /*************** LINK *******************/
  A: {
    MARKETPLACE_SIDEBAR: 'a[href="/marketplace"]',
  },

  /*************** BUTTON *******************/
  BUTTON: {
    NEW_DEAL: 'button[class="btn control-btn pmp"]',
  },

  /*************** INPUT *******************/
  INPUT: {
    CPM: '#cpm',
    MARGIN: '#margin',
    SHARE: '#share_of_voice',
    EXTERNAL: '#external_id',
    TIME_START: '#time_start',
    TIME_END: '#time_end',
  },

  /*************** LABEL *******************/
  LABEL: {
    GEN_AUTO: '#label-generate_external_id',
    ACTIVE: '#label-active',
    ONGOING: '#label-ongoing',
    DO_NOT_EXPIRE: '#label-do-not-expire',
  },
};