module.exports = {

  /*************** INPUT *******************/
  INPUT: {
    LOGIN_NAME: 'input[class="form-control"]',
    SEARCH_FIELD: 'input[id="search-119:20field"]',
    NAME: '#name',
    ENDPOINT_URL: '#endpoint_url',
    QPS_LIMIT: '#qps_limit',
  },

  /*************** DIV *******************/
  DIV: {
    REGION: '.region',
    TYPE: '.type',
    PROTOCOL: '.protocol_version',
  },

  /*************** BUTTON *******************/
  BUTTON: {
    NEW_DSP: 'button[class="btn control-btn advertisers"]',
    SAVE: 'button[type="submit"]',
  },

  /*************** LINK *******************/
  A: {
    DEMAND_SIDEBAR: 'a[href="/demand"]',
  },
  /*************** SPAN *******************/
  SPAN: {
    WELLCOME_NAME: 'span[class="header-dropdown__username"]',
  },

  /*************** TEXTAREA *******************/
  TEXTAREA: {
    COMMENTS: '.comments',
  },

  /*************** LABEL *******************/
  LABEL: {
    ACTIVE: '#label-active',
    BANNER: '#label-banner_traffic',
    VIDEO: '#label-video_traffic',
    NATIVE: '#label-native_traffic',
  },
};
