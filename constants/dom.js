module.exports = {
  DEVICE_TYPE_BOTH: '.modality .select-menu div:nth-child(3)',
  ZONE_TYPE_VIDEO: '.zone_type .select-menu div:nth-child(1)',
  AD_TYPE_DISPLAY: '.ad_type .select-menu div:nth-child(1)',
  AD_SERVING_FIRST: '.ad_serving .select-menu div:nth-child(1)',
  VIDEO_TYPE_FLV: '.video_type .select-menu div:nth-child(2)',
  DURATION_CHILD: '.video_duration .select-menu div:nth-child(2)',
  LIST_TYPE_COUNTRY: '.type .select-menu div:nth-child(1)',
  LIST_TYPE_DMA: '.type .select-menu div:nth-child(2)',
  LIST_TYPE_LL: '.type .select-menu div:nth-child(3)',
  LIST_TYPE_IP: '.type .select-menu div:nth-child(4)',
  LIST_TYPE_POSTAL_CODE: '.type .select-menu div:nth-child(5)',
  GEO_LIST_US: '.country .select-menu div:nth-child(1)',
  GEO_LIST_DMA_CODE_501: '.dma_code .select-menu div:nth-child(2)',
  TARGETING_TEMPLATE_SECTION_DOMAIN_LIST: '.is-clearable .select-menu div:nth-child(1)',

  /*************** INPUT *******************/
  INPUT: {
    LOGIN_NAME: 'input[class="form-control"]',
    PASSWORD: 'input[type="password"]',
    SEARCH_FIELD: 'input[id="search-field"]',
    SEARCH_AVAILABLE_ADS: 'input[id="categories-search"]',
    NAME: 'input[id="name"]',
    PUBLISHARE: 'input[id="publishershare"]',
    AFFILIATESHARE: 'input[id="affiliateshare"]',
    DOMAIN: '#domain',
    IP: '#ip',
  },

  /*************** DIV *******************/
  DIV: {
    NEW_COUNTRY: '.country',
    INFO_MESSAGE: 'div[class="noty_body"]',
    SIDEBAR_ICONS_NAME: 'div[class="sidebar-menu__item-name"]',
    DEVICE_TYPE: 'div[class="Select modality Select--single is-clearable has-value"]',
    ZONE_TYPE: 'div[class="Select zone_type Select--single is-clearable has-value"]',
    AD_TYPE: 'div[class="Select ad_type Select--single is-clearable has-value"]',
    AD_SERVING: 'div[class="Select ad_serving Select--single is-clearable has-value"]',
    LINK_AD: 'div[class="category-item-plus"]',
    ITEM_EXPAND: '.category-item-expand',
    NEXT_ITEM_EXPAND: '.category-item-expand',
    VIDEO_TYPE: '.video_type',
    DURATION: '.video_duration',
    LIST_TYPE: '.type',
    MODAL_FOOTER: '.modal-footer ',
    GEO_LIST_COUNTRY: '.country',
    GEO_LIST_DMA_CODE: '.dma_code',
    ADD_NEW_TARGETING: '.add-new-targeting',
    TARGETING_TEMPLATE_SECTION: '.is-clearable',
  },

  /*************** BUTTON *******************/
  BUTTON: {
    SUCCESS_CONFIRM_WINDOW: 'button[class="btn control-btn success confirm-btn"]',
    SUCCESS_CONFIRM_MODAL: 'button[class="btn control-btn success"]',
    SIGN_IN: 'button[id="sign-in"]',
    SAVE: 'button[type="submit"]',
    MODAL_WINDOW_SAVE: '',
    NEW_ADVERTISER: 'button[class="btn control-btn advertisers"]',
    NEW_CAMPAIGN: 'button[class="btn control-btn campaigns"]',
    NEW_PUBLISHER: 'button[class="btn control-btn publishers"]',
    NEW_SITE: 'button[class="btn control-btn sites"]',
    NEW_ZONE: 'button[class="btn control-btn zones"]',
    NEW_AD: 'button[class="btn control-btn ads"]',
    NEW_DOMAIN_LIST: '.global-list',
    NEW_GEO_LIST: 'button[class="btn control-btn geo-list"]',
    REMOVE_ALL: '.selected-categories-remove-all',
    NEW_ITEM: '#new_item',
    IMPORT_CSV: '#import_csv',
    NEW_TARGETING_TEMPLATE: 'button[class="btn control-btn targeting"]',
  },

  /*************** TD *******************/
  TD: {
    EMPTY_FIELD: 'td[class="empty-cell"]',
    DELETE_FIELD: 'td[class="text-center td-del td-opt"]',
    EDIT_FIELD: 'td[class="text-center td-edit td-opt"]',
  },

  /*************** LINK *******************/
  A: {
    DEMAND_SIDEBAR: 'a[href="/demand"]',
    SUPPLY_SIDEBAR: 'a[href="/supply"]',
    SITES: 'a[href="/supply/sites"]',
    CAMPAIGNS: 'a[href="/demand/campaigns"]',
    ZONES: 'a[href="/supply/zones"]',
    LINK_DEMAND: 'a[href="/supply/zones/131/link-demand"]',
    WATERFALL: 'a[href="/supply/zones/131/waterfall"]',
    TARGETING: 'a[href="/global-targeting"]',
    DOMAIN_LISTS: 'a[href="/global-targeting/domain-lists"]',
    GEO_LISTS: 'a[href="/global-targeting/geo-lists"]',
  },

  /*************** SPAN *******************/
  SPAN: {
    CANCEL_ICON: 'span[class="icon icon-Cancel_Icon"]',
    ARCHIVE_ICON: 'span[class="icon icon-Supply_Icon"]',
    WELLCOME_NAME: 'span[class="header-dropdown__username"]',
  },

  /*************** TEXTAREA *******************/
  TEXTAREA: {
    AD_CODE: '.tag-area',
    VAST_TAG: '#vast_tag',
    BANNER_CODE: '#html_template',
  },

  /*************** LABEL *******************/
  LABEL: {
    FORCE: 'label[for="mode-0"]',
    MERGE: 'label[for="mode-1"]',
    ARCHIVE: '#label-show_archived',
  },
};
