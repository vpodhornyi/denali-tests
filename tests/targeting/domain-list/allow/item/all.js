import conf from '../../../../../config/server';
import Demand from '../../../../../helper/demand';
import Targeting from '../../../../../helper/targeting';
import DomainList from '../../../../../helper/domain-list';
import CONST from "../../../../../constants";


fixture `Domain-list ALL Allow Item`.page(conf.goto);


test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create Targeting ALL', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'all',
    }
  );
});

test('Create Domain list ', async t => {

  await DomainList.create(
    t,
    {
      TYPE: 'allow',
    }
  );
});

test('Domain list add new Item', async t => {

  await DomainList.addItem(
    t,
    {
      DOMAIN: 'facebook.com',
    }
  );
});

test(CONST.COMPARE, async t => {
  await DomainList.compareSqlRedis(t);
});

test('Domain list merge import CSV', async t => {

  await DomainList.importCSV(
    t,
    {
      IMPORT_MODE: 'merge',
    }
  );
});

test(CONST.COMPARE, async t => {

  await DomainList.compareSqlRedis(t);
});

test('Domain list force import CSV', async t => {
  await DomainList.importCSV(
    t,
    {
      IMPORT_MODE: 'force',
    }
  );
});

test(CONST.COMPARE, async t => {

  await DomainList.compareSqlRedis(t);
});


test('Domain list edit Item', async t => {

  await DomainList.editItem(
    t,
    {
      DOMAIN: 'local.org',
    }
  );
});

test(CONST.COMPARE, async t => {

  await DomainList.compareSqlRedis(t);
});

test('Domain list delete Item', async t => {
  await DomainList.deleteItem(t);
});

test(CONST.COMPARE, async t => {
  await DomainList.compareSqlRedis(t);
});

test('Domain list delete last Item', async t => {
  await DomainList.deleteItem(t, { DOMAIN: 'ukr.net'});
});

test(CONST.COMPARE, async t => {
  await DomainList.compareSqlRedis(t);
});

test('Domain list merge import CSV big file', async t => {

  await DomainList.importCSV(
    t,
    {
      IMPORT_MODE: 'merge',
      FILE_NAME: 'many_domains.csv'
    }
  );
});

test('Domain list force import CSV big file', async t => {
  await DomainList.importCSV(
    t,
    {
      IMPORT_MODE: 'force',
      FILE_NAME: 'many_domains.csv'
    }
  );
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});
