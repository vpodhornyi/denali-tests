import conf from '../../../../config/server';
import Demand from '../../../../helper/demand';
import Targeting from '../../../../helper/targeting';
import DomainList from '../../../../helper/domain-list';

fixture `Domain-list DENY`.page(conf.goto);

test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});
// --------------------------------------------------------------------- ALL
test('Create Targeting ALL', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'all',
    }
  );
});

test('Create Domain list ', async t => {

  await DomainList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test('Delete Domain list ', async t => {
  await DomainList.delete(t);
});

// --------------------------------------------------------------------- BANNER
test('Create Targeting BANNER', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'banner',
    }
  );
});

test('Create Domain list ', async t => {

  await DomainList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test('Delete Domain list ', async t => {
  await DomainList.delete(t);
});

// --------------------------------------------------------------------- VIDEO
test('Create Targeting VIDEO', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'video',
    }
  );
});

test('Create Domain list ', async t => {

  await DomainList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test('Delete Domain list ', async t => {
  await DomainList.delete(t);
});

// --------------------------------------------------------------------- NATIVE
test('Create Targeting NATIVE', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'native',
    }
  );
});

test('Create Domain list ', async t => {

  await DomainList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test('Delete Domain list ', async t => {
  await DomainList.delete(t);
});
test('Delete DSP', async t => {
  await Demand.archived(t)
});
