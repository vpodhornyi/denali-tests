
import conf from '../../../../../config/server';
import Demand from '../../../../../helper/demand';
import Targeting from '../../../../../helper/targeting';
import IpList from '../../../../../helper/ip-list';
import CONST from "../../../../../constants";


fixture `Ip-list BANNER Deny Item`.page(conf.goto);


test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create Targeting BANNER', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'banner',
    }
  );
});

test('Create IP list ', async t => {

  await IpList.create(
    t,
    {
      TYPE: 'allow',
    }
  );
});

test('Ip list add new Item', async t => {

  await IpList.addItem(
    t,
    {
      IP: '192.168.0.99',
    }
  );
});

test(CONST.COMPARE, async t => {
  await IpList.compareSqlRedis(t);
});

test('Ip list merge import CSV', async t => {

  await IpList.importCSV(
    t,
    {
      IMPORT_MODE: 'merge',
    }
  );
});

test(CONST.COMPARE, async t => {
  await IpList.compareSqlRedis(t);
});

test('Ip list force import CSV', async t => {
  await IpList.importCSV(
    t,
    {
      IMPORT_MODE: 'force',
    }
  );
});

test(CONST.COMPARE, async t => {

  await IpList.compareSqlRedis(t);
});


test('Ip list edit Item', async t => {

  await IpList.editItem(
    t,
    {
      IP: '192.168.0.102',
    }
  );
});

test(CONST.COMPARE, async t => {
  await IpList.compareSqlRedis(t);
});

test('Ip list delete Item', async t => {
  await IpList.deleteItem(t);
});

test(CONST.COMPARE, async t => {
  await IpList.compareSqlRedis(t);
});

test('Ip list delete last Item', async t => {
  await IpList.deleteItem(t, { IP: '192.168.0.101'});
});

test(CONST.COMPARE, async t => {
  await IpList.compareSqlRedis(t);
});
test('Ip list merge import CSV big file', async t => {

  await IpList.importCSV(
    t,
    {
      IMPORT_MODE: 'merge',
      FILE_NAME: 'many_ips.csv'
    }
  );
});

test('Ip list force import CSV big file', async t => {
  await IpList.importCSV(
    t,
    {
      IMPORT_MODE: 'force',
      FILE_NAME: 'many_ips.csv'
    }
  );
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});

