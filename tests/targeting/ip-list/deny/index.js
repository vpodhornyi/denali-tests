import conf from '../../../../config/server';
import Demand from '../../../../helper/demand';
import Targeting from '../../../../helper/targeting';
import IpList from '../../../../helper/ip-list';
const del = 'Deleted Ip list';

fixture `Ip-list DENY`.page(conf.goto);

test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

// --------------------------------------------------------------------- ALL
test('Create Targeting ALL', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'all',
    }
  );
});

test('Create Ip list ', async t => {

  await IpList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await IpList.delete(t);
});

// --------------------------------------------------------------------- BANNER
test('Create Targeting BANNER', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'banner',
    }
  );
});

test('Create Ip list ', async t => {

  await IpList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await IpList.delete(t);
});

// --------------------------------------------------------------------- VIDEO
test('Create Targeting VIDEO', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'video',
    }
  );
});

test('Create Ip list ', async t => {

  await IpList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await IpList.delete(t);
});

// --------------------------------------------------------------------- NATIVE
test('Create Targeting NATIVE', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'native',
    }
  );
});

test('Create Ip list ', async t => {

  await IpList.create(
    t,
    {
      TYPE: 'deny',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await IpList.delete(t);
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});
