import conf from '../../../../config/server';
import Demand from '../../../../helper/demand';
import Targeting from '../../../../helper/targeting';
import GeoList from '../../../../helper/geo-list';
const del = 'Deleted Geo list';

fixture `Geo-list ALLOW`.page(conf.goto);

test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});
// --------------------------------------------------------------------- ALL
test('Create Targeting ALL', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'all',
    }
  );
});

test('Create Geo list ', async t => {

  await GeoList.create(
    t,
    {
      TYPE: 'allow',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await GeoList.delete(t);
});

// --------------------------------------------------------------------- BANNER
test('Create Targeting BANNER', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'banner',
    }
  );
});

test('Create Geo list ', async t => {

  await GeoList.create(
    t,
    {
      TYPE: 'allow',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await GeoList.delete(t);
});

// --------------------------------------------------------------------- VIDEO
test('Create Targeting VIDEO', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'video',
    }
  );
});

test('Create Geo list ', async t => {

  await GeoList.create(
    t,
    {
      TYPE: 'allow',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await GeoList.delete(t);
});

// --------------------------------------------------------------------- NATIVE
test('Create Targeting NATIVE', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'native',
    }
  );
});

test('Create Geo list ', async t => {

  await GeoList.create(
    t,
    {
      TYPE: 'allow',
      SCREEN_ON: 'on',
    }
  );
});

test(del, async t => {
  await GeoList.delete(t);
});
test('Delete DSP', async t => {
  await Demand.archived(t)
});
