import conf from '../../../../../config/server';
import Demand from '../../../../../helper/demand';
import Targeting from '../../../../../helper/targeting';
import GeoList from '../../../../../helper/geo-list';
import CONST from "../../../../../constants";


fixture `Geo-list NATIVE Deny Item`.page(conf.goto);


test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create Targeting ALL', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'native',
    }
  );
});

test('Create Domain list ', async t => {

  await GeoList.create(
    t,
    {
      TYPE: 'deny',
    }
  );
});

test('Geo list add new Item', async t => {
  await GeoList.addItem(t);
});

test(CONST.COMPARE, async t => {
  await GeoList.compareSqlRedis(t);
});


test('Geo list merge import CSV', async t => {

  await GeoList.importCSV(
    t,
    {
      IMPORT_MODE: 'merge',
    }
  );
});

test(CONST.COMPARE, async t => {
  await GeoList.compareSqlRedis(t);
});

test('Geo list force import CSV', async t => {
  await GeoList.importCSV(
    t,
    {
      IMPORT_MODE: 'force',
    }
  );
});

test(CONST.COMPARE, async t => {

  await GeoList.compareSqlRedis(t);
});


test('Geo list edit Item', async t => {
  await GeoList.editItem(t);
});

test(CONST.COMPARE, async t => {
  await GeoList.compareSqlRedis(t);
});

test('Geo list delete Item', async t => {
  await GeoList.deleteItem(t);
});

test(CONST.COMPARE, async t => {
  await GeoList.compareSqlRedis(t);
});

test('Geo list delete last Item', async t => {
  await GeoList.deleteItem(t, { COUNTRY: 'AX'});
});

test(CONST.COMPARE, async t => {
  await GeoList.compareSqlRedis(t);
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});
