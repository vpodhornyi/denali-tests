import conf from '../../../config/server';
import CONST from '../../../constants/index';
import Demand from '../../../helper/demand';
import Targeting from '../../../helper/targeting';

fixture `Targeting BANNER`.page(conf.goto);

test('Create DSP\n', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create Targeting', async t => {

  await Targeting.create(
    t,
    {
      TARGETING: 'banner',
      SCREEN_ON: 'on',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Targeting.compareSqlRedis(t);
});

test('Activate Targeting', async t => {

  await Targeting.active(
    t,
    {
      ACTIVE: 'activate',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Targeting.compareSqlRedis(t);
});

test('Deactivate Targeting', async t => {

  await Targeting.active(
    t,
    {
      ACTIVE: 'deactivate',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Targeting.compareSqlRedis(t);
});

test('Delete Targeting', async t => {
  await Targeting.delete(t);
});

test('Redis key check\n', async t => {
  await Targeting.redisDeleteCheck(t);
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});