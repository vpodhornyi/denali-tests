import conf from '../../config/server';
import CONST from '../../constants/index';
import Supply from '../../helper/supply';
import Demand from "../../helper/demand";


fixture `Supply create`
  .page(conf.goto);

//---------------------------------------------------------------------------------------------- Test-1
test('Test_1: type - Roku', async t => {
  await Supply.createSsp(
    t,
    {
      TYPE: 'Roku',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_1'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t);
});

//---------------------------------------------------------------------------------------------- Test-2
test('Test_2: type - Roku, no Comments', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
      QPS: '5',
      SCREEN_FILE_NAME: 'test_2'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3: type - Roku, no Qps limit', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
      SCREEN_FILE_NAME: 'test_3'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});

//---------------------------------------------------------------------------------------------- Test-4
test('Test_4: type - Roku, is Active ON', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
      SCREEN_FILE_NAME: 'test_4',
      ACTIVE: 'ON',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});


//---------------------------------------------------------------------------------------------- Test-5
test('Test_5: type - OpenRTB, Price enc method Base64', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      QPS: '5',
      COMMENT: 'new comment',
      PRICE_ENC_SECRET: 'some words',
      PRICE_ENC_METHOD: 'Base64',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_5',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});


//---------------------------------------------------------------------------------------------- Test-6
test('Test_6: type - OpenRTB, Price enc method BF', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      QPS: '5',
      COMMENT: 'new comment',
      PRICE_ENC_SECRET: 'some words',
      PRICE_ENC_METHOD: 'BF',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_6',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});

//---------------------------------------------------------------------------------------------- Test-7
test('Test_7: type - OpenRTB, no type Comments', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      QPS: '5',
      PRICE_ENC_SECRET: 'some words',
      PRICE_ENC_METHOD: 'BF',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_7',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});


//---------------------------------------------------------------------------------------------- Test-8
test('Test_8: type - OpenRTB, no type QPS', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      PRICE_ENC_SECRET: 'some words',
      PRICE_ENC_METHOD: 'BF',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_8',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});

//---------------------------------------------------------------------------------------------- Test-9
test('Test_9: type - OpenRTB, is Active ON', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      PRICE_ENC_SECRET: 'some words',
      PRICE_ENC_METHOD: 'BF',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_9',
      ACTIVE: 'ON'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//--------------------------------------------------------------------------------------------- Test-10
test('Test_10: type - OpenRTB, no type Price enc secret', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      PRICE_ENC_METHOD: 'BF',
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_10',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});

//--------------------------------------------------------------------------------------------- Test-11
test('Test_11: type - OpenRTB, no type Currencies', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      PRICE_ENC_METHOD: 'BF',
      SCREEN_FILE_NAME: 'test_11',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});
