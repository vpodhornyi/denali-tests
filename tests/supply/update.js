import conf from '../../config/server';
import CONST from '../../constants/index';
import Supply from '../../helper/supply';
import Demand from "../../helper/demand";


fixture `Supply update`
  .page(conf.goto);


test('Create SSP type - Roku\n', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
    }
  );
});

//---------------------------------------------------------------------------------------------- Test-1
test('Test_1: Type Roku update Comments', async t => {
  await Supply.updateSsp(
    t,
    {
      COMMENTS: 'New Year soon!',
      SCREEN_FILE_NAME: 'test_1'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-2
test('Test_2: Type Roku update QPS', async t => {
  await Supply.updateSsp(
    t,
    {
      QPS: '5',
      SCREEN_FILE_NAME: 'test_2'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3: Type Roku  update Is Active', async t => {
  await Supply.updateSsp(
    t,
    {
      ACTIVE: 'ON',
      SCREEN_FILE_NAME: 'test_3'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3_1: Type Roku  Is Active OFF', async t => {
  await Supply.updateSsp(
    t,
    {
      ACTIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_3_1'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-4
test('Test_4: Type Roku  update Demand margin', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_MARGIN: '5',
      SCREEN_FILE_NAME: 'test_4'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-5
test('Test_5: Type Roku  update Supply margin', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_MARGIN: '7',
      SCREEN_FILE_NAME: 'test_5'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-6
test('Test_6: Type Roku  update Default bid floor', async t => {
  await Supply.updateSsp(
    t,
    {
      DEFAULT_BID_FLOOR: '9',
      SCREEN_FILE_NAME: 'test_6',
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});


test('Create SSP type - OpenRTB\n', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'OpenRTB',
      PRICE_ENC_METHOD: 'Base64',
    }
  );
});


//---------------------------------------------------------------------------------------------- Test-7
test('Test_7: Type OpenRTB update Comments', async t => {
  await Supply.updateSsp(
    t,
    {
      COMMENTS: 'New Year very soon!',
      SCREEN_FILE_NAME: 'test_7'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-8
test('Test_8: Type OpenRTB update QPS', async t => {
  await Supply.updateSsp(
    t,
    {
      QPS: '6',
      SCREEN_FILE_NAME: 'test_8'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-9
test('Test_9: Type OpenRTB  update Is Active', async t => {
  await Supply.updateSsp(
    t,
    {
      ACTIVE: 'ON',
      SCREEN_FILE_NAME: 'test_9'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-9_1
test('Test_9_1: Type OpenRTB  Is Active OFF', async t => {
  await Supply.updateSsp(
    t,
    {
      ACTIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_9_1'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-10
test('Test_10: Type OpenRTB  update Protocol version', async t => {
  await Supply.updateSsp(
    t,
    {
      PROTOCOL: '2.3',
      SCREEN_FILE_NAME: 'test_10'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-11
test('Test_11: Type OpenRTB  update Price enc method (BF)', async t => {
  await Supply.updateSsp(
    t,
    {
      PRICE_ENC_METHOD: 'BF',
      SCREEN_FILE_NAME: 'test_11'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-11
test('Test_11: Type OpenRTB  update Price enc method (BF)', async t => {
  await Supply.updateSsp(
    t,
    {
      PRICE_ENC_METHOD: 'BF',
      SCREEN_FILE_NAME: 'test_11'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-12
test('Test_12: Type OpenRTB  update Price enc secret', async t => {
  await Supply.updateSsp(
    t,
    {
      PRICE_ENC_SECRET: 'new price',
      SCREEN_FILE_NAME: 'test_12'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-13
test('Test_13: Type OpenRTB  update Currencies', async t => {
  await Supply.updateSsp(
    t,
    {
      CURRENCIES: 'USA',
      SCREEN_FILE_NAME: 'test_13'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-14
test('Test_14: Type OpenRTB  update margin Demand ctv', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_CTV: '2',
      SCREEN_FILE_NAME: 'test_14'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-15
test('Test_15: Type OpenRTB  update margin Demand Desktop site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_DESKTOP_SITE_BANNER: '3',
      SCREEN_FILE_NAME: 'test_15'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-16
test('Test_16: Type OpenRTB  update margin Demand Desktop  site video', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_DESKTOP_SITE_VIDEO: '4',
      SCREEN_FILE_NAME: 'test_16'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-17
test('Test_17: Type OpenRTB  update margin Demand Desktop  app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_DESKTOP_APP_BANNER: '5',
      SCREEN_FILE_NAME: 'test_17'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-18
test('Test_18: Type OpenRTB  update margin Demand Desktop  app video', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_DESKTOP_APP_VIDEO: '6',
      SCREEN_FILE_NAME: 'test_18'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-19
test('Test_19: Type OpenRTB  update margin Demand Desktop  site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_MOBILE_SITE_BANNER: '7',
      SCREEN_FILE_NAME: 'test_19'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-20
test('Test_20: Type OpenRTB  update margin Demand Desktop  site video', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_MOBILE_SITE_VIDEO: '8',
      SCREEN_FILE_NAME: 'test_20'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-21
test('Test_21: Type OpenRTB  update margin Demand Mobile app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_MOBILE_APP_BANNER: '9',
      SCREEN_FILE_NAME: 'test_21'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-22
test('Test_22: Type OpenRTB  update margin Demand Mobile app video', async t => {
  await Supply.updateSsp(
    t,
    {
      DEMAND_MOBILE_APP_VIDEO: '10',
      SCREEN_FILE_NAME: 'test_22'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-23
test('Test_23: Type OpenRTB  update margin Supply CTV', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_CTV: '11',
      SCREEN_FILE_NAME: 'test_23'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-24
test('Test_24: Type OpenRTB  update margin Supply  Desktop site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_DESKTOP_SITE_BANNER: '12',
      SCREEN_FILE_NAME: 'test_24'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-25
test('Test_25: Type OpenRTB  update margin Supply  Desktop site video', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_DESKTOP_SITE_VIDEO: '13',
      SCREEN_FILE_NAME: 'test_25'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-26
test('Test_26: Type OpenRTB  update margin Supply  Desktop app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_DESKTOP_APP_BANNER: '14',
      SCREEN_FILE_NAME: 'test_26'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-27
test('Test_27: Type OpenRTB  update margin Supply  Desktop app video', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_DESKTOP_APP_VIDEO: '15',
      SCREEN_FILE_NAME: 'test_27'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-28
test('Test_28: Type OpenRTB  update margin Supply  Mobile site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_MOBILE_SITE_BANNER: '16',
      SCREEN_FILE_NAME: 'test_28'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-29
test('Test_29: Type OpenRTB  update margin Supply  Mobile site video', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_MOBILE_SITE_VIDEO: '17',
      SCREEN_FILE_NAME: 'test_29'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-30
test('Test_30: Type OpenRTB  update margin Supply  Mobile app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_MOBILE_APP_BANNER: '18',
      SCREEN_FILE_NAME: 'test_30'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-31
test('Test_31: Type OpenRTB  update margin Supply  Mobile app video', async t => {
  await Supply.updateSsp(
    t,
    {
      SUPPLY_MOBILE_APP_VIDEO: '19',
      SCREEN_FILE_NAME: 'test_31'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-32
test('Test_32: Type OpenRTB  update margin Bid Floor CTV', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_CTV: '20',
      SCREEN_FILE_NAME: 'test_32'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-33
test('Test_33: Type OpenRTB  update margin Bid Floor Desktop site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_DESKTOP_SITE_BANNER: '21',
      SCREEN_FILE_NAME: 'test_33'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-34
test('Test_34: Type OpenRTB  update margin Bid Floor Desktop site video', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_DESKTOP_SITE_VIDEO: '22',
      SCREEN_FILE_NAME: 'test_34'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-35
test('Test_35: Type OpenRTB  update margin Bid Floor Desktop app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_DESKTOP_APP_BANNER: '23',
      SCREEN_FILE_NAME: 'test_35'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-36
test('Test_36: Type OpenRTB  update margin Bid Floor Desktop app video', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_DESKTOP_APP_VIDEO: '24',
      SCREEN_FILE_NAME: 'test_36'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-37
test('Test_37: Type OpenRTB  update margin Bid Floor Mobile site banner', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_MOBILE_SITE_BANNER: '25',
      SCREEN_FILE_NAME: 'test_37'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-38
test('Test_38: Type OpenRTB  update margin Bid Floor Mobile site video', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_MOBILE_SITE_VIDEO: '26',
      SCREEN_FILE_NAME: 'test_38'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-39
test('Test_39: Type OpenRTB  update margin Bid Floor Mobile app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_MOBILE_APP_BANNER: '27',
      SCREEN_FILE_NAME: 'test_39'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-40
test('Test_40: Type OpenRTB  update margin Bid Floor Mobile app banner', async t => {
  await Supply.updateSsp(
    t,
    {
      BID_FLOOR_MOBILE_APP_VIDEO: '28',
      SCREEN_FILE_NAME: 'test_40'
    }
  );
});

test(CONST.COMPARE, async t => {
  await Supply.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Supply.archived(t)
});
