import conf from '../../config/server';
import CONST from '../../constants/index';
import Demand from '../../helper/demand';

fixture `Demand update`
  .page(conf.goto);

test('Create DSP AdSever ASIA\n', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

//---------------------------------------------------------------------------------------------- Test-1
test('Test_1: update QPS limit', async t => {

  await Demand.updateDsp(
    t,
    {
      QPS: '5',
      SCREEN_FILE_NAME: 'test_1'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_2: update Comments', async t => {

  await Demand.updateDsp(
    t,
    {
      COMMENTS: 'new comments',
      SCREEN_FILE_NAME: 'test_2'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_3: update Endpoint URL', async t => {

  await Demand.updateDsp(
    t,
    {
      URL: 'http://local.com',
      SCREEN_FILE_NAME: 'test_3'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_4: update is Active', async t => {

  await Demand.updateDsp(
    t,
    {
      ACTIVE: 'ON',
      SCREEN_FILE_NAME: 'test_4'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});


test('Test_4_1: is Active OFF', async t => {

  await Demand.updateDsp(
    t,
    {
      ACTIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_4_1'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_5: update Banner traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      BANNER: 'OFF',
      SCREEN_FILE_NAME: 'test_5'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_6: update VIDEO traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      VIDEO: 'OFF',
      SCREEN_FILE_NAME: 'test_6'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_7: update NATIVE traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      NATIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_7'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_8: update region EUROPE', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'EUROPE',
      SCREEN_FILE_NAME: 'test_8'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_9: update region US_EAST', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'US_EAST',
      SCREEN_FILE_NAME: 'test_9'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_10: update region US_WEST', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'US_WEST',
      SCREEN_FILE_NAME: 'test_10'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_11: update region ASIA', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'ASIA',
      SCREEN_FILE_NAME: 'test_11'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_12: update dsp Name', async t => {

  await Demand.updateDsp(
    t,
    {
      DSP_NAME: Demand.getRandomName(),
      SCREEN_FILE_NAME: 'test_12'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});


test('Create DSP OpenRTB ASIA\n', async t => {

  await Demand.createDsp(t,
    {
      REGION: 'ASIA',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
    }
  );
});

test('Test_13: update QPS limit', async t => {

  await Demand.updateDsp(
    t,
    {
      QPS: '5',
      SCREEN_FILE_NAME: 'test_13'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_14: update Comments', async t => {

  await Demand.updateDsp(
    t,
    {
      COMMENTS: 'new comments',
      SCREEN_FILE_NAME: 'test_14'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_15: update Endpoint URL', async t => {

  await Demand.updateDsp(
    t,
    {
      URL: 'http://local.com',
      SCREEN_FILE_NAME: 'test_15'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_16: update is Active ON', async t => {

  await Demand.updateDsp(
    t,
    {
      ACTIVE: 'ON',
      SCREEN_FILE_NAME: 'test_16'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});


test('Test_16_1: is Active OFF', async t => {

  await Demand.updateDsp(
    t,
    {
      ACTIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_16_1'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_17: update Banner traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      BANNER: 'OFF',
      SCREEN_FILE_NAME: 'test_17'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_18: update VIDEO traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      VIDEO: 'OFF',
      SCREEN_FILE_NAME: 'test_18'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_19: update NATIVE traffic', async t => {

  await Demand.updateDsp(
    t,
    {
      NATIVE: 'OFF',
      SCREEN_FILE_NAME: 'test_19'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_20: update region EUROPE', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'EUROPE',
      SCREEN_FILE_NAME: 'test_20'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_21: update region US_EAST', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'US_EAST',
      SCREEN_FILE_NAME: 'test_21'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_22: update region US_WEST', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'US_WEST',
      SCREEN_FILE_NAME: 'test_22'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_23: update region ASIA', async t => {

  await Demand.updateDsp(
    t,
    {
      REGION: 'ASIA',
      SCREEN_FILE_NAME: 'test_23'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_24: update dsp Name', async t => {

  await Demand.updateDsp(
    t,
    {
      DSP_NAME: Demand.getRandomName(),
      SCREEN_FILE_NAME: 'test_24'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Delete', async t => {
  await Demand.archived(t)
});