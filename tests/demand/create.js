import conf from '../../config/server';
import CONST from '../../constants/index';
import Demand from '../../helper/demand';

fixture `Demand create`
  .page(conf.goto);

//---------------------------------------------------------------------------------------------- Test-1

test('Test_1: type - AdServer, region - ASIA', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_1',
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-2
test('Test_2: type - AdServer, region - EUROPE', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'EUROPE',
      TYPE: 'AdServer',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_2',
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3: type - AdServer, region - US_EAST', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_EAST',
      TYPE: 'AdServer',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_3'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-4
test('Test_4: type - AdServer, region - US_WEST', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'AdServer',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_4'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-5
test('Test_5: type - OpenRTB, region - ASIA', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'ASIA',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_5'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-6
test('Test_6: type - OpenRTB, region - EUROPE', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'EUROPE',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_6'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-7
test('Test_7: type - OpenRTB, region - US_EAST', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_EAST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_7'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-8
test('Test_8: type - OpenRTB, region - US_WEST', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      QPS: '5',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_8'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-9
test('Test_9: type - AdServer, region - ASIA no type QPS', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      COMMENT: 'new comment',
      SCREEN_FILE_NAME: 'test_9'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-10
test('Test_10: type - AdServer, region - ASIA no type Comments', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      QPS: '5',
      SCREEN_FILE_NAME: 'test_10'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-11
test('Test_11: type - AdServer, region - ASIA Is Active ON', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      SCREEN_FILE_NAME: 'test_11',
      ACTIVE: 'ON',
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test('Test_11_1: update is not Active', async t => {

  await Demand.updateDsp(
    t,
    {
      ACTIVE: 'OFF',
      SCREEN_FILE_NAME: 'createDSP_test_11_1'
    },
  );
});

test('------- Not archived', async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-12
test('Test_12: type - AdServer, region - ASIA Banner traffic Off', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      BANNER: 'Off',
      SCREEN_FILE_NAME: 'test_12'
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-13
test('Test_13: type - AdServer, region - ASIA Video traffic Off', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      VIDEO: 'Off',
      SCREEN_FILE_NAME: 'test_13',
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-14
test('Test_14: type - AdServer, region - ASIA Native traffic Off', async t => {

  await Demand.createDsp(
    t,
    {
      REGION: 'US_WEST',
      TYPE: 'OpenRTB',
      URL: 'http://google.com',
      NATIVE: 'Off',
      SCREEN_FILE_NAME: 'test_14',
    },
  );
});

test(CONST.COMPARE, async t => {
  await Demand.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await Demand.archived(t)
});

test(CONST.DELETE, async t => {
  await Demand.delete(t)
});