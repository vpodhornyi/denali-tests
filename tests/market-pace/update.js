import conf from '../../config/server';
import CONST from '../../constants/index';
import MarketPlace from '../../helper/market-plase';
import Demand from '../../helper/demand';
import Supply from '../../helper/supply';


fixture `Market-place update`.page(conf.goto);

test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {

      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create SSP', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
    }
  );
});

test('Create newDSP', async t => {

  await Demand.createDsp(
    t,
    {

      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create newSSP', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
    }
  );
});

test('Create Deal',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'update_Deal',
      }
    );
  });

//---------------------------------------------------------------------------------------------- Test-1
test('Test_1: update DSP',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        DSP: Demand.getRandomName(),
        SCREEN_FILE_NAME: 'test_1',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-2
test('Test_2: update SSP',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        SSP: Supply.getRandomName(),
        SCREEN_FILE_NAME: 'test_2',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3: update Active',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        ACTIVE: 'ON',
        SCREEN_FILE_NAME: 'test_3',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-4
test('Test_4: update CPM',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        CPM: '2',
        SCREEN_FILE_NAME: 'test_4',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-5
test('Test_5: update Margin',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        MARGIN: '21',
        SCREEN_FILE_NAME: 'test_5',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-6
test('Test_6: update Share of voice',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        SHARE: '11',
        SCREEN_FILE_NAME: 'test_6',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-7
test('Test_7: update Start now',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        START_NOW: 'off',
        SCREEN_FILE_NAME: 'test_7',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-8
test('Test_8: update Don\'t expire',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        DONT_EXPIRE: 'off',
        SCREEN_FILE_NAME: 'test_8',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-9
test('Test_9: update Active',
  async t => {
    await MarketPlace.updateMarketplace(
      t,
      {
        ACTIVE: 'OFF',
        SCREEN_FILE_NAME: 'test_9',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test('Delete Deal', async t => {
  await MarketPlace.delete(t)
});

