import conf from '../../config/server';
import CONST from '../../constants/index';
import MarketPlace from '../../helper/market-plase';
import Demand from '../../helper/demand';
import Supply from '../../helper/supply';


fixture `Market-place create`.page(conf.goto);

test('Create DSP', async t => {

  await Demand.createDsp(
    t,
    {

      REGION: 'ASIA',
      TYPE: 'AdServer',
      URL: 'http://google.com',
    }
  );
});

test('Create SSP\n', async t => {
  await Supply.createSsp(
    t,
    {

      TYPE: 'Roku',
    }
  );
});

//---------------------------------------------------------------------------------------------- Test-1
test('Test_1: External ID generate automatically',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_1'
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await MarketPlace.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-2
test('Test_2: External ID type manual',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        EXTERNAL_ID: 'manual',
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_2',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await MarketPlace.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-3
test('Test_3: Active ON',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        ACTIVE: 'ON',
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_3',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

//---------------------------------------------------------------------------------------------- Test-4
test('Test_4: Start now OFF',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        START_NOW: 'off',
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_4',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await MarketPlace.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-5
test('Test_5: Don’t expire OFF',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        DONT_EXPIRE: 'off',
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_5',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await MarketPlace.delete(t)
});

//---------------------------------------------------------------------------------------------- Test-6
test('Test_6: Start now and Don’t expire OFF',
  async t => {
    await MarketPlace.createMarketplace(
      t,
      {
        DONT_EXPIRE: 'off',
        START_NOW: 'off',
        CPM: '12',
        MARGIN: '10',
        SHARE: '25',
        SCREEN_FILE_NAME: 'test_6',
      }
    );
  });

test(CONST.COMPARE, async t => {
  await MarketPlace.compareSqlRedis(t);
});

test(CONST.ARCHIVED, async t => {
  await MarketPlace.delete(t)
});

test('Delete DSP', async t => {
  await Demand.archived(t)
});


test('Delete SSP', async t => {
  await Supply.archived(t)
});

