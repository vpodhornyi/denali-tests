import {Selector, ClientFunction} from 'testcafe';
import conf from "../config/server";
import CONST from "../constants";
import KEY from "../constants/redis-keys";
import DOM from "../constants/dom";
import Targeting from "./targeting";
import {sqlDomains} from "../database/domain-list";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class DomainsList extends Targeting {


  static async compareSqlRedis(t) {

    const sql = this.getArrFromObj(await sqlDomains(this.domainId)).sort();
    const redis = this.getArrFromObj(await client.smembersAsync(this.domainRedisKey)).sort();

    let fieldRedis = await client.hgetAsync(this.targRedisKey, `${this.type}.domain`);

    const sqlRedis = sql.length > 0 ? this.domainId : '0';

    let str = 'redis -    '
      + JSON.stringify(redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql)
      + '\n'
      + 'targetingRedisKey - '
      + this.targRedisKey
      + '\n'
      + 'field - '
      + `${this.type}.domain - ${fieldRedis}`
      + '\n'
      + 'domainRedisKey - '
      + this.domainRedisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/domain-list/${this.type}/item/${this.targType}/${this.actionItem}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis).eql(sql)
      .expect(fieldRedis).eql(sqlRedis);
  }

  static async delete(t) {

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDelete))
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);

    await t
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/${this.targType}_delete.png`);

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Domain list has been deleted`)

  }

  static async create(t, testData) {

    this.type = testData.TYPE;
    this.buttonEdit = Selector(`#${this.targType}-edit`);
    this.buttonCreate = Selector(`#${this.type}-domain-lists-create`);
    this.buttonDomainListEdit = Selector(`#${this.type}-domain-lists-edit`);
    this.buttonDelete = Selector(`#${this.type}-domain-lists-delete`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonCreate))

    if (testData.SCREEN_ON === 'on') {
      await t
        .wait(500)
        .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/${this.targType}_create.png`);
    }

    this.domainId = await ClientFunction(() => window.location.href.split('/')[10])();
    this.domainRedisKey = KEY.DOMAIN + this.domainId;

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Domain list has been created`)
  }

  static async addItem(t, testData) {

    this.domain = testData.DOMAIN;
    this.actionItem = 'add';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDomainListEdit))
      .click(DOM.BUTTON.NEW_ITEM)
      .typeText(DOM.INPUT.DOMAIN, this.domain)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/item/${this.targType}/add.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Domain list item has been added`)
  }

  static async editItem(t, testData) {

    this.actionItem = 'edit';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDomainListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, 'google.com')
      .click(DOM.TD.EDIT_FIELD)
      .click(DOM.INPUT.DOMAIN)
      .pressKey('ctrl+a delete')
      .typeText(DOM.INPUT.DOMAIN, testData.DOMAIN)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete');

    this.domain = testData.DOMAIN;

    await t
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/item/${this.targType}/edit.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Domain list item has been updated`)
  }

  static async deleteItem(t, testData) {

    this.actionItem = 'delete';
    let deleteDomain = this.domain;
    let add = '';

    if(testData){
      deleteDomain = testData.DOMAIN;
      add = 'last_Ip_';
      this.actionItem = add + 'delete';
    }

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDomainListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, deleteDomain)
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete')
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/item/${this.targType}/${add}delete.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Domain list item has been deleted`);
  }

  static async importCSV(t, testData) {

    const info = Selector(DOM.DIV.INFO_MESSAGE).innerText;
    this.importMode = testData.IMPORT_MODE;
    this.actionItem = 'import_' + this.importMode;
    let fileName = 'two-domains.csv';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDomainListEdit))
      .click(DOM.BUTTON.IMPORT_CSV);

    if (this.importMode === 'merge') {
      await t
        .click(DOM.LABEL.MERGE);
    }

    if (testData.FILE_NAME) {
      fileName = testData.FILE_NAME;
      this.actionItem = 'import_' + this.importMode + '_big_file';
    }

    await t
      .setFilesToUpload('input[type="file"]', [`../../../../../csv/${fileName}`])
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .expect(info)
      .eql(`File "${fileName}" has been imported`)
      .takeScreenshot(`${this.getTestTime()}/domain-list/${this.type}/item/${this.targType}/${this.actionItem}.png`)
  }
}