import {Selector, ClientFunction} from 'testcafe';
import CONST from "../constants/index";
import conf from "../config/server";
import Common from './common';
import {sqlSsp} from "../database/supply";
import DOM from "../constants/dom";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);


export default class Supply extends Common {

  static async redisDeleteCheck(t) {
    const redis = await client.hgetallAsync(this.redisKey);
    await t
      .expect(null)
      .eql(redis);
  }

  static async compareSqlRedis(t) {

    const sql = await sqlSsp(this.sspId, this.sspType);
    const redis = await client.hgetallAsync(this.redisKey);

    let str = 'redis -    ' + JSON.stringify(redis ? this.sortObj(redis) : redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql ? this.sortObj(sql) : sql)
      + '\n'
      + 'redisKey - '
      + this.redisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/supply/${this.action}/${this.test_number}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis)
      .eql(sql);
  }

  static async archived(t, data) {
    await this.login(t, conf.login, conf.password);

    let name = this.sspName;

    if(data && data.DELETE_NAME){
      name = conf.testName + data.DELETE_NAME;
    }

    await t
      .click(CONST.SSP.A.SUPPLY_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, name)
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);

    if(this.test_number) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/supply/${this.action}/${this.test_number}_archived.png`)
    }

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText).eql(`SSP "${name}" has been archived`)
  }

  static async createSsp(t, testData) {

    await this.login(t, conf.login, conf.password);

    let type = this.sspType = testData.TYPE;
    this.action = 'create';
    this.sspName = conf.testName + (testData.SSP_NAME  || this.getRandomName());
    this.redisKey = CONST.KEYS.SSP + this.sspName;

    switch (type) {
      case 'OpenRTB':
        type = '1';
        break;
      case 'Roku':
        type = '2';
        break;
    }

    await t
      .click(CONST.SSP.A.SUPPLY_SIDEBAR)
      .click(CONST.SSP.A.SSPS)
      .click(CONST.SSP.BUTTON.NEW_SSP)
      .typeText(CONST.DSP.INPUT.NAME, this.sspName)
      .click(CONST.SSP.DIV.PUBLISHER)
      .click(this.isClearable(1))
      .click(CONST.DSP.DIV.TYPE)
      .click(this.isClearable(type));



    if (testData.QPS) {
      await t
        .typeText(CONST.DSP.INPUT.QPS_LIMIT, testData.QPS)
    }

    if (testData.COMMENT) {
      await t
        .typeText(CONST.DSP.TEXTAREA.COMMENTS, testData.COMMENT)
    }

    if (testData.ACTIVE) {
      await t
        .click(CONST.DSP.LABEL.ACTIVE);
    }

    if (type === '1') {
      await t
        .click(CONST.DSP.DIV.PROTOCOL)
        .click(this.isClearable(1));

      if (testData.PRICE_ENC_SECRET) {
        await t
          .typeText(CONST.SSP.INPUT.PRICE_ENC_SECRET, testData.PRICE_ENC_SECRET)
      }

      if (testData.CURRENCIES) {
        await t
          .click(CONST.SSP.DIV.CURRENCIES)
          .click(this.isClearable(1));
      }

      if (testData.PRICE_ENC_METHOD === 'Base64') {
        await t
          .click(CONST.SSP.DIV.PRICE_ENC_METHOD)
          .click(this.isClearable(1))
      } else if (testData.PRICE_ENC_METHOD === 'BF') {
        await t
          .click(CONST.SSP.DIV.PRICE_ENC_METHOD)
          .click(this.isClearable(2))
      }
    }

    await t
      .click(CONST.DOM.BUTTON.SAVE);

    if (testData.SCREEN_FILE_NAME) {
      this.test_number = testData.SCREEN_FILE_NAME;
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/supply/${this.action}/${this.test_number}.png`);
    }

    this.sspId = await ClientFunction(() => window.location.href.split('/')[5])();

    await t
      .expect(Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`SSP "${this.sspName}" has been created`);

  }

  static async updateSsp(t, testData) {

    this.action = 'update';
    
    await this.login(t, conf.login, conf.password);
    await t
      .click(CONST.SSP.A.SUPPLY_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.sspName);
    
    const sspName = Selector('a').withText(this.sspName);
    
    await t
      .click(sspName);

    if (testData.COMMENTS) {
      await t
        .click(CONST.SSP.TEXTAREA.COMMENTS)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.TEXTAREA.COMMENTS, testData.COMMENTS);
    }

    if (testData.QPS) {
      await t
        .click(CONST.DSP.INPUT.QPS_LIMIT)
        .pressKey('ctrl+a delete')
        .typeText(CONST.DSP.INPUT.QPS_LIMIT, testData.QPS);
    }

    if (testData.ACTIVE) {
      await t
        .click(CONST.DSP.LABEL.ACTIVE);
    }

    if (testData.DEMAND_MARGIN) {
      await t
        .click(CONST.SSP.INPUT.DEMAND_MARGIN)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_MARGIN, testData.DEMAND_MARGIN);
    }

    if (testData.SUPPLY_MARGIN) {
      await t
        .click(CONST.SSP.INPUT.SUPPLY_MARGIN)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_MARGIN, testData.SUPPLY_MARGIN);
    }

    if (testData.DEFAULT_BID_FLOOR) {
      await t
        .click(CONST.SSP.INPUT.DEFAULT_BID_FLOOR)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEFAULT_BID_FLOOR, testData.DEFAULT_BID_FLOOR);
    }

    if (testData.PROTOCOL) {
      await t
        .click(CONST.SSP.DIV.PROTOCOL)
        .click(this.isClearable(2));
    }

    if (testData.PRICE_ENC_METHOD) {
      await t
        .click(CONST.SSP.DIV.PRICE_ENC_METHOD)
        .click(this.isClearable(2));
    }

    if (testData.PRICE_ENC_SECRET) {
      await t
        .click(CONST.SSP.INPUT.PRICE_ENC_SECRET)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.PRICE_ENC_SECRET, testData.PRICE_ENC_SECRET);
    }

    if (testData.CURRENCIES) {
      await t
        .click(CONST.SSP.DIV.CURRENCIES)
        .click(this.isClearable(1));
    }

    if (testData.DEMAND_CTV) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.LABEL.DEFAULT_VALUES)
        .click(CONST.SSP.INPUT.DEMAND_CTV)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_CTV, testData.DEMAND_CTV)
    }

    if (testData.DEMAND_DESKTOP_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_DESKTOP_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_DESKTOP_SITE_BANNER, testData.DEMAND_DESKTOP_SITE_BANNER)
    }

    if (testData.DEMAND_DESKTOP_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_DESKTOP_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_DESKTOP_SITE_VIDEO, testData.DEMAND_DESKTOP_SITE_VIDEO)
    }

    if (testData.DEMAND_DESKTOP_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_DESKTOP_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_DESKTOP_APP_BANNER, testData.DEMAND_DESKTOP_APP_BANNER)
    }
    
    if (testData.DEMAND_DESKTOP_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_DESKTOP_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_DESKTOP_APP_VIDEO, testData.DEMAND_DESKTOP_APP_VIDEO)
    }

    if (testData.DEMAND_MOBILE_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_MOBILE_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_MOBILE_SITE_BANNER, testData.DEMAND_MOBILE_SITE_BANNER)
    }

    if (testData.DEMAND_MOBILE_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_MOBILE_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_MOBILE_SITE_VIDEO, testData.DEMAND_MOBILE_SITE_VIDEO)
    }

    if (testData.DEMAND_MOBILE_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_MOBILE_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_MOBILE_APP_BANNER, testData.DEMAND_MOBILE_APP_BANNER)
    }

    if (testData.DEMAND_MOBILE_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.DEMAND_MOBILE_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.DEMAND_MOBILE_APP_VIDEO, testData.DEMAND_MOBILE_APP_VIDEO)
    }

    if (testData.SUPPLY_CTV) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_CTV)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_CTV, testData.SUPPLY_CTV)
    }

    if (testData.SUPPLY_DESKTOP_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_DESKTOP_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_DESKTOP_SITE_BANNER, testData.SUPPLY_DESKTOP_SITE_BANNER)
    }
    
    if (testData.SUPPLY_DESKTOP_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_DESKTOP_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_DESKTOP_SITE_VIDEO, testData.SUPPLY_DESKTOP_SITE_VIDEO)
    }

    if (testData.SUPPLY_DESKTOP_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_DESKTOP_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_DESKTOP_APP_BANNER, testData.SUPPLY_DESKTOP_APP_BANNER)
    }

    if (testData.SUPPLY_DESKTOP_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_DESKTOP_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_DESKTOP_APP_VIDEO, testData.SUPPLY_DESKTOP_APP_VIDEO)
    }

    if (testData.SUPPLY_MOBILE_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_MOBILE_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_MOBILE_SITE_BANNER, testData.SUPPLY_MOBILE_SITE_BANNER)
    }

    if (testData.SUPPLY_MOBILE_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_MOBILE_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_MOBILE_SITE_VIDEO, testData.SUPPLY_MOBILE_SITE_VIDEO)
    }

    if (testData.SUPPLY_MOBILE_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_MOBILE_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_MOBILE_APP_BANNER, testData.SUPPLY_MOBILE_APP_BANNER)
    }

    if (testData.SUPPLY_MOBILE_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.SUPPLY_MOBILE_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.SUPPLY_MOBILE_APP_VIDEO, testData.SUPPLY_MOBILE_APP_VIDEO)
    }

    if (testData.BID_FLOOR_CTV) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_CTV)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_CTV, testData.BID_FLOOR_CTV)
    }

    if (testData.BID_FLOOR_DESKTOP_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_SITE_BANNER, testData.BID_FLOOR_DESKTOP_SITE_BANNER)
    }

    if (testData.BID_FLOOR_DESKTOP_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_SITE_VIDEO, testData.BID_FLOOR_DESKTOP_SITE_VIDEO)
    }

    if (testData.BID_FLOOR_DESKTOP_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_APP_BANNER, testData.BID_FLOOR_DESKTOP_APP_BANNER)
    }

    if (testData.BID_FLOOR_DESKTOP_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_DESKTOP_APP_VIDEO, testData.BID_FLOOR_DESKTOP_APP_VIDEO)
    }

    if (testData.BID_FLOOR_MOBILE_SITE_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_MOBILE_SITE_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_MOBILE_SITE_BANNER, testData.BID_FLOOR_MOBILE_SITE_BANNER)
    }

    if (testData.BID_FLOOR_MOBILE_SITE_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_MOBILE_SITE_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_MOBILE_SITE_VIDEO, testData.BID_FLOOR_MOBILE_SITE_VIDEO)
    }

    if (testData.BID_FLOOR_MOBILE_APP_BANNER) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_MOBILE_APP_BANNER)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_MOBILE_APP_BANNER, testData.BID_FLOOR_MOBILE_APP_BANNER)
    }

    if (testData.BID_FLOOR_MOBILE_APP_VIDEO) {
      await t
        .click(`a[href="/supply/ssps/${this.sspId}/margins"]`)
        .click(CONST.SSP.INPUT.BID_FLOOR_MOBILE_APP_VIDEO)
        .pressKey('ctrl+a delete')
        .typeText(CONST.SSP.INPUT.BID_FLOOR_MOBILE_APP_VIDEO, testData.BID_FLOOR_MOBILE_APP_VIDEO)
    }

    await t
      .click(CONST.DOM.BUTTON.SAVE);

    if (testData.SCREEN_FILE_NAME) {
      this.test_number = testData.SCREEN_FILE_NAME;
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/supply/${this.action}/${this.test_number}.png`);
    }

    await t
      .expect(Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`SSP "${this.sspName}" has been updated`);
  }
}