import {Selector, ClientFunction, hasChildElements} from 'testcafe';
import CONST from "../constants/index";
import conf from "../config/server";
import Common from './common';
import DOM from "../constants/dom";
import {sqlDsp} from "../database/demand";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class Demand extends Common {

  static async redisDeleteCheck(t) {

    let res = [];
    let commonRes = [];
    let str = '';

    fs.writeFile(
      `screenshots/${this.getTestTime()}/demand/${this.action}/${this.test_number}_delete_keys_check.txt`,
      str,
      function (err) {
      });

    await t
      .expect(commonRes)
      .eql(res);
  }

  static async compareSqlRedis(t) {

    const sql = await sqlDsp(this.dspId, this.dspType);
    const redis = await client.hgetallAsync(this.dspRedisKey);

    let str = 'redis -    '
      + JSON.stringify(redis ? this.sortObj(redis) : redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql ? this.sortObj(sql) : sql)
      + '\n'
      + 'redisKey - '
      + this.dspRedisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/demand/${this.action}/${this.test_number}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis)
      .eql(sql);
  }

  static async delete(t) {
    await this.login(t, conf.login, conf.password);

    let name = this.dspName;

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .click(CONST.DOM.LABEL.ARCHIVE)
      .typeText(DOM.INPUT.SEARCH_FIELD, name);

    if (!(await Selector(DOM.SPAN.CANCEL_ICON).exists)) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}_not_delete.png`);
      return;
    }

    await t
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW)
      .wait(1000)
      .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}_deleted.png`)
  }

  static async archived(t, data) {

    await this.login(t, conf.login, conf.password);

    let name = this.dspName;

    if (data && data.DELETE_NAME) {
      name = conf.testName + data.DELETE_NAME;
    }

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, name)
      .wait(1000);

    if (!(await Selector(DOM.SPAN.ARCHIVE_ICON).exists)) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}_not_archived.png`);
      return;
    }
    await t
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);

    if (this.test_number) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}_archived.png`)
    }

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText).eql(`DSP "${name}" has been archived`)
  }

  static async createDsp(t, testData) {
    await this.login(t, conf.login, conf.password);

    this.dspType = testData.TYPE;
    this.action = 'create';

    this.dspName = conf.testName + (testData.DSP_NAME || this.getRandomName());

    let type = null;
    let region = null;

    switch (testData.TYPE) {
      case 'AdServer':
        type = '1';
        break;
      case 'OpenRTB':
        type = '2';
        break;
    }

    switch (testData.REGION) {
      case 'ASIA':
        region = 1;
        break;
      case 'EUROPE':
        region = 2;
        break;
      case 'US_EAST':
        region = 3;
        break;
      case 'US_WEST':
        region = 4;
        break;
      default:
        region = 1;
        break;
    }

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .click(CONST.DSP.BUTTON.NEW_DSP)
      .typeText(CONST.DSP.INPUT.NAME, this.dspName)
      .click(CONST.DSP.DIV.TYPE)
      .click(this.isClearable(type))
      .click(CONST.DSP.DIV.REGION)
      .click(this.isClearable(region));

    if (type === '2') {
      await t
        .click(CONST.DSP.DIV.PROTOCOL)
        .click(this.isClearable(1))
    }

    await t
      .typeText(CONST.DSP.INPUT.ENDPOINT_URL, testData.URL);

    if (testData.QPS) {
      await t
        .typeText(CONST.DSP.INPUT.QPS_LIMIT, testData.QPS)
    }

    if (testData.COMMENT) {
      await t
        .typeText(CONST.DSP.TEXTAREA.COMMENTS, testData.COMMENT)
    }

    if (testData.ACTIVE) {
      await t
        .click(CONST.DSP.LABEL.ACTIVE);
    }

    if (testData.BANNER) {
      await t
        .click(CONST.DSP.LABEL.BANNER);
    }

    if (testData.VIDEO) {
      await t
        .click(CONST.DSP.LABEL.VIDEO);
    }

    if (testData.NATIVE) {
      await t
        .click(CONST.DSP.LABEL.NATIVE);
    }

    await t
      .click(CONST.DOM.BUTTON.SAVE);

    this.test_number = testData.SCREEN_FILE_NAME || 0;

    if (testData.SCREEN_FILE_NAME) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}.png`);
    }


    this.dspId = await ClientFunction(() => window.location.href.split('/')[5])();
    this.dspRedisKey = CONST.KEYS.DSP + this.dspId;

    await t
      .expect(Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`DSP "${this.dspName}" has been created`);
  }

  static async updateDsp(t, testData) {

    this.action = 'update';
    this.test_number = testData.SCREEN_FILE_NAME;


    let region = null;

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName);

    const dspName = Selector('a').withText(this.dspName);

    await t
      .click(dspName);

    switch (testData.REGION) {
      case 'ASIA':
        region = 1;
        break;
      case 'EUROPE':
        region = 2;
        break;
      case 'US_EAST':
        region = 3;
        break;
      case 'US_WEST':
        region = 4;
        break;
    }

    if (region) {
      await t
        .click(CONST.DSP.DIV.REGION)
        .click(this.isClearable(region));
    }

    if (testData.DSP_NAME) {
      this.dspName = conf.testName + testData.DSP_NAME;
      await t
        .click(CONST.DSP.INPUT.NAME)
        .pressKey('ctrl+a delete')
        .typeText(CONST.DSP.INPUT.NAME, this.dspName)
    }

    if (testData.ACTIVE) {
      await t
        .click(CONST.DSP.LABEL.ACTIVE);
    }

    if (testData.BANNER === 'OFF') {
      await t
        .click(CONST.DSP.LABEL.BANNER);
    }

    if (testData.VIDEO === 'OFF') {
      await t
        .click(CONST.DSP.LABEL.VIDEO);
    }

    if (testData.NATIVE === 'OFF') {
      await t
        .click(CONST.DSP.LABEL.NATIVE);
    }

    if (testData.URL) {
      await t
        .click(CONST.DSP.INPUT.ENDPOINT_URL)
        .pressKey('ctrl+a delete')
        .typeText(CONST.DSP.INPUT.ENDPOINT_URL, testData.URL);
    }

    if (testData.COMMENTS) {
      await t
        .click(CONST.DSP.TEXTAREA.COMMENTS)
        .pressKey('ctrl+a delete')
        .typeText(CONST.DSP.TEXTAREA.COMMENTS, testData.COMMENTS)
    }

    if (testData.QPS) {
      await t
        .click(CONST.DSP.INPUT.QPS_LIMIT)
        .pressKey('ctrl+a delete')
        .typeText(CONST.DSP.INPUT.QPS_LIMIT, testData.QPS);
    }

    await t
      .click(CONST.DOM.BUTTON.SAVE);

    if (testData.SCREEN_FILE_NAME) {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/demand/${this.action}/${this.test_number}.png`);
    }

    await t
      .expect(Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText).eql(`DSP "${ this.dspName}" has been updated`);
  }
}