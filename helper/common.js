import DOM from '../constants/dom';
import randomStreang from "randomstring";

const date = new Date();
const testTime = date;

export default class Common {

  static getTestTime() {
    return testTime.toISOString().split(':').join('');
  }

  static getRandomName() {
    return randomStreang.generate(12);
  }
  
  static async login(t, login, password) {

    await t
      .typeText(DOM.INPUT.LOGIN_NAME, login)
      .typeText(DOM.INPUT.PASSWORD, password)
      .pressKey('enter')
  }

  static sortObj(data) {
    let obj = {};
    const sortedKeys = Object.keys(data).sort();
    sortedKeys.forEach(key => obj[key] = data[key]);
    return obj;
  }
  
  static getArrFromObj(obj){
    let arr = [];
    for(let key in obj){
      arr.push(obj[key]);
    }
    return arr;
  }

  static isClearable(child) {
    return `.is-clearable .select-menu div:nth-child(${child})`;
  }

  static isSearchable(child) {
    return `.is-searchable .select-menu div:nth-child(${child})`;
  }
}
