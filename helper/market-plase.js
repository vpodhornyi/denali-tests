import {Selector, ClientFunction} from 'testcafe';
import CONST from "../constants/index";
import conf from "../config/server";
import randomStreang from "randomstring";
import Common from './common';
import {sqlMarketPlace} from "../database/market-place";
import DOM from "../constants/dom";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class MarketPlace extends Common {

  static async redisDeleteCheck(t){
    const redis = await client.hgetallAsync(this.redisKey);
    await t
      .expect(null)
      .eql(redis);
  }

  static async compareSqlRedis(t) {

    const sql = await sqlMarketPlace(this.dealId);
    const redis = await client.hgetallAsync( this.redisKey);

    let str = 'redis -    ' 
      + JSON.stringify(redis ? this.sortObj(redis) : redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql ? this.sortObj(sql) : sql)
      + '\n'
      + 'redisKey - '
      +  this.redisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/market-place/${this.action}/${this.test_number}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis)
      .eql(sql);
  }

  static async delete(t) {
    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.MP.A.MARKETPLACE_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.externalId)
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);
 

      if(this.test_number){
        await t
          .wait(1000)
          .takeScreenshot(`${this.getTestTime()}/market-place/${this.action}/${this.test_number}_archived.png`)
      }

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText).eql(`Deal "${this.externalId}" has been archived`)
  }

  static async createMarketplace(t, testData) {

    await this.login(t, conf.login, conf.password);

    this.action = 'create';

    await t
      .click(CONST.MP.A.MARKETPLACE_SIDEBAR)
      .click(CONST.MP.BUTTON.NEW_DEAL)
      .click(CONST.MP.DIV.DSP)
      .click( this.isClearable(1))
      .click(CONST.MP.DIV.SSP)
      .click( this.isClearable(1))
      .click(CONST.MP.INPUT.CPM)
      .pressKey('ctrl+a delete')
      .typeText(CONST.MP.INPUT.CPM, testData.CPM)
      .click(CONST.MP.INPUT.MARGIN)
      .pressKey('ctrl+a delete')
      .typeText(CONST.MP.INPUT.MARGIN, testData.MARGIN)
      .click(CONST.MP.INPUT.SHARE)
      .pressKey('ctrl+a delete')
      .typeText(CONST.MP.INPUT.SHARE, testData.SHARE);

    if(testData.EXTERNAL_ID === 'manual'){
      await t
        .click(CONST.MP.LABEL.GEN_AUTO)
        .typeText(CONST.MP.INPUT.EXTERNAL, await randomStreang.generate(16));
    }

    if(testData.ACTIVE){
      await t
        .click(CONST.MP.LABEL.ACTIVE);
    }

    if(testData.START_NOW === 'off'){
      await t
        .click(CONST.MP.LABEL.ONGOING);
    }

    if(testData.DONT_EXPIRE === 'off'){
      await t
        .click(CONST.MP.LABEL.DO_NOT_EXPIRE);
    }

    await t
      .click(CONST.DOM.BUTTON.SAVE);

    if (testData.SCREEN_FILE_NAME) {
      this.test_number = testData.SCREEN_FILE_NAME;
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/market-place/${this.action}/${this.test_number}.png`);
    }

    const infoMessage = await Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText;

    await t
      .wait(1000);

    this.dealId = await ClientFunction(() => window.location.href.split('/')[5])();
    this.redisKey = CONST.KEYS.DEAL + this.dealId;
    this.externalId = await Selector(CONST.MP.INPUT.EXTERNAL).value;

    await t
      .expect(infoMessage).eql(`Deal "${this.externalId}" has been created`);
  }

  static async updateMarketplace(t, testData) {

    this.action = 'update';
    
    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.MP.A.MARKETPLACE_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.externalId);
    
    const externalId = Selector('a').withText(this.externalId);

    await t
      .click(externalId);

    if(testData.DSP === 'newDSP'){
      await t
        .click(CONST.MP.DIV.DSP)
        .click( this.isClearable(2))
    }

    if(testData.SSP === 'newSSP'){
      await t
        .click(CONST.MP.DIV.SSP)
        .click( this.isClearable(2))
    }

    if(testData.ACTIVE){
      await t
        .click(CONST.MP.LABEL.ACTIVE);
    }

    if(testData.CPM){
      await t
        .click(CONST.MP.INPUT.CPM)
        .pressKey('ctrl+a delete')
        .typeText(CONST.MP.INPUT.CPM, testData.CPM)
    }

    if(testData.MARGIN){
      await t
        .click(CONST.MP.INPUT.MARGIN)
        .pressKey('ctrl+a delete')
        .typeText(CONST.MP.INPUT.MARGIN, testData.MARGIN)
    }

    if(testData.SHARE){
      await t
        .click(CONST.MP.INPUT.SHARE)
        .pressKey('ctrl+a delete')
        .typeText(CONST.MP.INPUT.SHARE, testData.SHARE)
    }

    if(testData.START_NOW === 'off'){
      await t
        .click(CONST.MP.LABEL.ONGOING);
    }

    if(testData.DONT_EXPIRE === 'off'){
      await t
        .click(CONST.MP.LABEL.DO_NOT_EXPIRE);
    }
    
    
    await t
      .click(CONST.DOM.BUTTON.SAVE);

    if (testData.SCREEN_FILE_NAME) {
      this.test_number = testData.SCREEN_FILE_NAME;
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/market-place/${this.action}/${this.test_number}.png`);
    }

    const infoMessage = await Selector(CONST.DOM.DIV.INFO_MESSAGE).innerText;

    await t
      .expect(infoMessage).eql(`Deal "${this.externalId}" has been updated`);
  }
}