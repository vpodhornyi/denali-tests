import {Selector, ClientFunction} from 'testcafe';
import conf from "../config/server";
import CONST from "../constants";
import DOM from "../constants/dom";
import Targeting from "./targeting";
import {sqlCountries} from "../database/geo-list";
import KEY from "../constants/redis-keys";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class GeoList extends Targeting {

  static async compareSqlRedis(t) {

    const sql = await sqlCountries(this.geoId);
    const redis = await client.hgetAsync(this.targRedisKey, `${this.type}.geo.country`);

    const sqlRedis = sql.length > 0 ? sql.join(',') : '';

    let str = 'redis -    '
      + JSON.stringify(redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sqlRedis)
      + '\n'
      + 'targetingRedisKey - '
      + this.targRedisKey
      + '\n'
      + 'field - '
      + `${this.type}.geo.country`;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/geo-list/${this.type}/item/${this.targType}/${this.actionItem}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis).eql(sqlRedis)
  }

  static async delete(t) {

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDelete))
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);

    await t
      .wait(1000)
      .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/${this.targType}_delete.png`);

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Geo list has been deleted`)

  }

  static async create(t, testData) {

    this.type = testData.TYPE;
    this.buttonEdit = Selector(`#${this.targType}-edit`);
    this.buttonCreate = Selector(`#${this.type}-geo-lists-create`);
    this.buttonGeoListEdit = Selector(`#${this.type}-geo-lists-edit`);
    this.buttonDelete = Selector(`#${this.type}-geo-lists-delete`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonCreate))

    if (testData.SCREEN_ON === 'on') {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/${this.targType}_create.png`);
    }

    this.geoId = await ClientFunction(() => window.location.href.split('/')[10])();

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Geo list has been created`)
  }

  static async addItem(t) {

    this.actionItem = 'add';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonGeoListEdit))
      .click(DOM.BUTTON.NEW_ITEM)
      .click(CONST.DOM.DIV.NEW_COUNTRY)
      .click(this.isClearable(2))
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .wait(1000)
      .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/item/${this.targType}/add.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Geo list item has been added`)
  }

  static async editItem(t) {

    this.actionItem = 'edit';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonGeoListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, 'Af')
      .click(DOM.TD.EDIT_FIELD)
      .click(CONST.DOM.DIV.NEW_COUNTRY)
      .click(this.isClearable(6))
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete')
      .wait(1000)
      .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/item/${this.targType}/edit.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Geo list item has been updated`)
  }

  static async deleteItem(t, testData) {

    this.actionItem = 'delete';
    let deleteCountry = 'DZ';
    let add = '';

    if(testData){
      deleteCountry = testData.COUNTRY;
      add = 'last_country_';
      this.actionItem = add + 'delete';
    }

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonGeoListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, deleteCountry)
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete')
      .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/item/${this.targType}/${add}delete.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Geo list item has been deleted`)

  }

  static async importCSV(t, testData) {

    this.importMode = testData.IMPORT_MODE;
    this.actionItem = 'import_' + this.importMode;
    let fileName = 'two-countries.csv';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonGeoListEdit))
      .click(DOM.BUTTON.IMPORT_CSV);

    if (this.importMode === 'merge') {
      await t
        .click(DOM.LABEL.MERGE);
    }

    if (testData.FILE_NAME) {
      fileName = testData.FILE_NAME;
      this.actionItem = 'import_' + this.importMode + '_big_file';
    }

    await t
      .setFilesToUpload('input[type="file"]', [`../../../../../csv/${fileName}`])
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`File "${fileName}" has been imported`)
      .takeScreenshot(`${this.getTestTime()}/geo-list/${this.type}/item/${this.targType}/${this.actionItem}.png`)
  }
}