import {Selector, ClientFunction} from 'testcafe';
import conf from "../config/server";
import CONST from "../constants";
import DOM from "../constants/dom";
import Targeting from "./targeting";
import {sqlIps} from "../database/ip-list";
import KEY from "../constants/redis-keys";

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class IpList extends Targeting {

  static async compareSqlRedis(t) {

    const sql = this.getArrFromObj(await sqlIps(this.ipId)).sort();
    const redis = this.getArrFromObj(await client.smembersAsync(this.ipRedisKey)).sort();
    const fieldRedis = await client.hgetAsync(this.targRedisKey, `${this.type}.ip`);
    const sqlRedis = sql.length > 0 ? this.ipId : '0';

    let str = 'redis -    '
      + JSON.stringify(redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql)
      + '\n'
      + 'targetingRedisKey - '
      + this.targRedisKey
      + '\n'
      + 'field - '
      + `${this.type}.ip - ${fieldRedis}`
      + '\n'
      + 'ipRedisKey - '
      + this.ipRedisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/ip-list/${this.type}/item/${this.targType}/${this.actionItem}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis).eql(sql)
      .expect(fieldRedis).eql(sqlRedis);
  }

  static async delete(t) {

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonDelete))
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW)
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/${this.targType}_delete.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`IP list has been deleted`)
  }

  static async create(t, testData) {

    this.type = testData.TYPE;
    this.buttonEdit = Selector(`#${this.targType}-edit`);
    this.buttonCreate = Selector(`#${this.type}-ip-lists-create`);
    this.buttonIpListEdit = Selector(`#${this.type}-ip-lists-edit`);
    this.buttonDelete = Selector(`#${this.type}-ip-lists-delete`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonCreate))

    if (testData.SCREEN_ON === 'on') {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/${this.targType}_create.png`);
    }

    this.ipId = await ClientFunction(() => window.location.href.split('/')[10])();
    this.ipRedisKey = KEY.IP + this.ipId;

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`IP list has been created`)
  }

  static async addItem(t, testData) {

    this.ip = testData.IP;
    this.actionItem = 'add';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonIpListEdit))
      .click(DOM.BUTTON.NEW_ITEM)
      .typeText(DOM.INPUT.IP, this.ip)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/item/${this.targType}/add.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`IP list item has been added`)

  }

  static async editItem(t, testData) {

    this.actionItem = 'edit';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonIpListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, '192.168.0.100')
      .click(DOM.TD.EDIT_FIELD)
      .click(DOM.INPUT.IP)
      .pressKey('ctrl+a delete')
      .typeText(DOM.INPUT.IP, testData.IP)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete');

    this.ip = testData.IP;

    await t
      .wait(500)
      .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/item/${this.targType}/edit.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`IP list item has been updated`)

  }

  static async deleteItem(t, testData) {

    this.actionItem = 'delete';
    let deleteIp = this.ip;
    let add = '';

    if(testData){
      deleteIp = testData.IP;
      add = 'last_Ip_';
      this.actionItem = add + 'delete';
    }

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonIpListEdit))
      .typeText(DOM.INPUT.SEARCH_FIELD, deleteIp)
      .click(DOM.TD.DELETE_FIELD)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW)
      .click(DOM.INPUT.SEARCH_FIELD)
      .pressKey('ctrl+a delete')
      .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/item/${this.targType}/${add}delete.png`)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`IP list item has been deleted`)

  }

  static async importCSV(t, testData) {

    this.importMode = testData.IMPORT_MODE;
    this.actionItem = 'import_' + this.importMode;
    let fileName = 'two-ips.csv';

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(Selector(this.battonTargeting))
      .click(Selector(this.buttonEdit))
      .click(Selector(this.buttonIpListEdit))
      .click(DOM.BUTTON.IMPORT_CSV);

    if (this.importMode === 'merge') {
      await t
        .click(DOM.LABEL.MERGE);
    }

    if (testData.FILE_NAME) {
      fileName = testData.FILE_NAME;
      this.actionItem = 'import_' + this.importMode + '_big_file';
    }

    await t
      .setFilesToUpload('input[type="file"]', [`../../../../../csv/${fileName}`])
      .click(DOM.BUTTON.SUCCESS_CONFIRM_MODAL)
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`File "${fileName}" has been imported`)
      .takeScreenshot(`${this.getTestTime()}/ip-list/${this.type}/item/${this.targType}/${this.actionItem}.png`)
  }
}