import {Selector, ClientFunction} from 'testcafe';
import {sqlTargeting} from "../database/targeting";
import conf from "../config/server";
import CONST from "../constants";
import KEY from "../constants/redis-keys";
import DOM from "../constants/dom";
import Demand from './demand';

let fs = require("fs");

const redis = require('redis');
const client = redis.createClient(conf.redis);
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export default class Targeting extends Demand {

  static async redisDeleteCheck(t) {

    const redis = await client.hgetallAsync(this.targRedisKey);

    let str = `${this.targRedisKey} - ${redis}`;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/targeting/${this.targType}/${this.action}_key_check.txt`,
      str,
      function (err) {
      });

    await t
      .expect(null)
      .eql(redis);
  }

  static async compareSqlRedis(t) {

    const sql = await sqlTargeting(this.dspId, this.targType);
    const redis = await client.hgetallAsync(this.targRedisKey);

    let str = 'redis -    '
      + JSON.stringify(redis ? this.sortObj(redis) : redis)
      + '\n'
      + 'sql -      ' + JSON.stringify(sql ? this.sortObj(sql) : sql)
      + '\n'
      + 'redisKey - '
      + this.targRedisKey;

    fs.writeFile(
      `screenshots/${this.getTestTime()}/targeting/${this.targType}/${this.action}_compare.txt`,
      str,
      function (err) {
      });

    await t
      .expect(redis)
      .eql(sql);
  }

  static async delete(t) {

    this.action = 'delete';
    const del = Selector(`#${this.targType}-delete`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(this.battonTargeting)
      .click(del)
      .click(DOM.BUTTON.SUCCESS_CONFIRM_WINDOW);


      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/targeting/${this.targType}/delete.png`);

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Targeting "${this.targType}" has been deleted`)
  }

  static async create(t, testData) {

    this.action = 'create';
    this.targType = (testData.TARGETING);
    this.targRedisKey = KEY.TARGETING + `${this.dspId}:${this.targType.toUpperCase() }`;
    this.battonTargeting = Selector(`a[href="/demand/dsps/${this.dspId}/targeting"]`);
    this.buttonTargetingCreate = Selector(`#${this.targType}-create`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(this.battonTargeting)
      .click(this.buttonTargetingCreate);

    if(testData.SCREEN_ON === 'on') {
      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/targeting/${this.targType}/create.png`);
    }

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Targeting "${this.targType}" has been created`)

  }

  static async active(t, testData) {

    this.action = testData.ACTIVE;
    const active = Selector(`#${this.targType}-${testData.ACTIVE}`);

    await this.login(t, conf.login, conf.password);

    await t
      .click(CONST.DSP.A.DEMAND_SIDEBAR)
      .typeText(DOM.INPUT.SEARCH_FIELD, this.dspName)
      .click(this.battonTargeting )
      .click(active);

      await t
        .wait(1000)
        .takeScreenshot(`${this.getTestTime()}/targeting/${this.targType}/${testData.ACTIVE}.png`);

    await t
      .expect(Selector(DOM.DIV.INFO_MESSAGE).innerText)
      .eql(`Targeting "${this.targType}" has been ${testData.ACTIVE}d`)
  }
}